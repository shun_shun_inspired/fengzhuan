/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(Object.prototype.hasOwnProperty.call(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"cart2": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push([6,"vendor"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js/components/flatpickr.js":
/*!************************************!*\
  !*** ./js/components/flatpickr.js ***!
  \************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var flatpickr__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! flatpickr */ "../node_modules/flatpickr/dist/esm/index.js");
/* harmony import */ var _node_modules_flatpickr_dist_flatpickr_min_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../node_modules/flatpickr/dist/flatpickr.min.css */ "../node_modules/flatpickr/dist/flatpickr.min.css");
/* harmony import */ var _node_modules_flatpickr_dist_flatpickr_min_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_flatpickr_dist_flatpickr_min_css__WEBPACK_IMPORTED_MODULE_1__);


$(document).ready(function () {
  var fp = Object(flatpickr__WEBPACK_IMPORTED_MODULE_0__["default"])("#time", {
    dateFormat: 'Y/m/d',
    minDate: $('#delivery_date').text()
  });
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "../node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./js/components/validate.js":
/*!***********************************!*\
  !*** ./js/components/validate.js ***!
  \***********************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(jQuery) {/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery */ "../node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var jquery_validation__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! jquery-validation */ "../node_modules/jquery-validation/dist/jquery.validate.js");
/* harmony import */ var jquery_validation__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(jquery_validation__WEBPACK_IMPORTED_MODULE_1__);


jquery__WEBPACK_IMPORTED_MODULE_0___default.a.extend(jQuery.validator.messages, {
  required: "必填",
  email: "請正確填寫您的 E-mail",
  number: "請輸入數字"
});
jquery__WEBPACK_IMPORTED_MODULE_0___default.a.validator.addMethod("isMobile", function (value, element) {
  var length = value.length;
  var second_num = parseInt(value.split('')[1]);
  return this.optional(element) || length == 10 && second_num == 9;
}, "請正確填寫您的手機號碼");
jquery__WEBPACK_IMPORTED_MODULE_0___default()(document).ready(function () {
  jquery__WEBPACK_IMPORTED_MODULE_0___default()('form.cart').validate({
    rules: {
      reci_email: {
        email: true
      },
      order_email: {
        email: true
      },
      reci_phone: {
        isMobile: true,
        number: true
      },
      order_phone: {
        isMobile: true,
        number: true
      }
    },
    errorElement: "em",
    errorClass: 'form-invalid',
    highlight: function highlight(element, errorClass) {
      jquery__WEBPACK_IMPORTED_MODULE_0___default()(element).removeClass(errorClass);
    },
    errorPlacement: function errorPlacement(error, element) {
      if (element.attr('type') == 'radio' || element.attr('type') == 'checkbox') {
        error.appendTo(jquery__WEBPACK_IMPORTED_MODULE_0___default()(element).parent().parent());
      } else if (/zip/.test(element.attr('name'))) {
        error.appendTo(jquery__WEBPACK_IMPORTED_MODULE_0___default()(element).parent().parent().parent());
        jquery__WEBPACK_IMPORTED_MODULE_0___default()(element).parent().css({
          'margin-bottom': 0
        });
        error.css({
          'margin-bottom': '10px'
        });
      } else if (/address/.test(element.attr('name'))) {
        error.appendTo(jquery__WEBPACK_IMPORTED_MODULE_0___default()(element).parent().parent());
      } else {
        error.insertAfter(element);
      }
    }
  });
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "../node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./js/components/zone_box.js":
/*!***********************************!*\
  !*** ./js/components/zone_box.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(jQuery) {// JavaScript Document

/* 使用方法



$(document).zone_box({

	MAIN_ID : "", // 縣市元素 ID (select 元素)

	SUB_ID : "", // 區域元素 ID (select 元素)

	ZIP_ID : "", // 郵遞區號 ID (input 元素)

	ZIP_GET : "", // 輸入郵遞區號

},function(ZIP){

	//callback

});



*/
(function ($) {
  $.fn.zone_box = function (OPTION, CALLBACK) {
    var ZONE = jQuery.extend({
      MAIN_ID: "",
      // 縣市元素 ID (select 元素)
      SUB_ID: "",
      // 區域元素 ID (select 元素)
      ZIP_ID: "",
      // 郵遞區號 ID (input 元素)
      ZIP_GET: "",
      // 輸入郵遞區號
      DEFAULT_AREA: "",
      // 預設區域
      //----
      MAIN: "",
      SUB: "",
      CODE: "",
      MAIN_STR: "",
      SUB_STR: "",
      REINPUT: false,
      RESELECT: false
    }, OPTION);
    ZONE.MAIN = new Array("請選擇", "台北市", "新北市", "基隆市", "宜蘭縣", "桃園市", "新竹市", "新竹縣", "苗栗縣", "台中市", "彰化縣", "南投縣", "嘉義市", "嘉義縣", "雲林縣", "台南市", "高雄市", "澎湖縣", "屏東縣", "台東縣", "花蓮縣", "金門縣", "連江縣"); //全省各縣市

    ZONE.SUB = new Array(23);
    ZONE.SUB[0] = new Array("請選擇"); //請選擇 

    ZONE.SUB[1] = new Array("中正區", "大同區", "中山區", "松山區", "大安區", "萬華區", "信義區", "士林區", "北投區", "內湖區", "南港區", "文山區"); //台北市 

    ZONE.SUB[2] = new Array("萬里區", "金山區", "板橋區", "汐止區", "深坑區", "石碇區", "瑞芳區", "平溪區", "雙溪區", "貢寮區", "新店區", "坪林區", "烏來區", "永和區", "中和區", "土城區", "三峽區", "樹林區", "鶯歌區", "三重區", "新莊區", "泰山區", "林口區", "蘆洲區", "五股區", "八里區", "淡水區", "三芝區", "石門區"); //新北市

    ZONE.SUB[3] = new Array("仁愛區", "信義區", "中正區", "中山區", "安樂區", "暖暖區", "七堵區"); //基隆市 

    ZONE.SUB[4] = new Array("宜蘭市", "頭城鎮", "礁溪鄉", "壯圍鄉", "員山鄉", "羅東鎮", "三星鄉", "大同鄉", "五結鄉", "冬山鄉", "蘇澳鎮", "南澳鄉", "釣魚台列嶼"); //宜蘭縣 

    ZONE.SUB[5] = new Array("中壢區", "平鎮區", "龍潭區", "楊梅區", "新屋區", "觀音區", "桃園區", "龜山區", "八德區", "大溪區", "復興區", "大園區", "蘆竹區"); //桃園市 

    ZONE.SUB[6] = new Array("東區", "北區", "香山區"); //新竹市 

    ZONE.SUB[7] = new Array("竹北市", "湖口鄉", "新豐鄉", "新埔鄉", "關西鎮", "芎林鄉", "寶山鄉", "竹東鎮", "五峰鄉", "橫山鄉", "尖石鄉", "北埔鄉", "峨嵋鄉"); //新竹縣 

    ZONE.SUB[8] = new Array("竹南鎮", "頭份市", "三灣鄉", "南庄鄉", "獅潭鄉", "後龍鎮", "通霄鎮", "苑裡鎮", "苗栗市", "造橋鄉", "頭屋鄉", "公館鄉", "大湖鄉", "泰安鄉", "鉰鑼鄉", "三義鄉", "西湖鄉", "卓蘭鄉"); //苗栗縣

    ZONE.SUB[9] = new Array("中區", "東區", "南區", "西區", "北區", "北屯區", "西屯區", "南屯區", "太平區", "大里區", "霧峰區", "烏日區", "豐原區", "后里區", "石岡區", "東勢區", "和平區", "新社區", "潭子區", "大雅區", "神岡區", "大肚區", "沙鹿區", "龍井區", "梧棲區", "清水區", "大甲區", "外埔區", "大安區"); //台中市 

    ZONE.SUB[10] = new Array("彰化市", "芬園鄉", "花壇鄉", "秀水鄉", "鹿港鎮", "福興鄉", "線西鄉", "和美鎮", "伸港鄉", "員林市", "社頭鄉", "永靖鄉", "埔心鄉", "溪湖鎮", "大村鄉", "埔鹽鄉", "田中鎮", "北斗鎮", "田尾鄉", "埤頭鄉", "溪州鄉", "竹塘鄉", "二林鎮", "大城鄉", "芳苑鄉", "二水鄉"); //彰化縣

    ZONE.SUB[11] = new Array("南投市", "中寮鄉", "草屯鎮", "國姓鄉", "埔里鎮", "仁愛鄉", "名間鄉", "集集鄉", "水里鄉", "魚池鄉", "信義鄉", "竹山鎮", "鹿谷鄉"); //南投縣 

    ZONE.SUB[12] = new Array("東區", "西區"); //嘉義市 

    ZONE.SUB[13] = new Array("番路鄉", "梅山鄉", "竹崎鄉", "阿里山鄉", "中埔鄉", "大埔鄉", "水上鄉", "鹿草鄉", "太保市", "朴子市", "東石鄉", "六腳鄉", "新港鄉", "民雄鄉", "大林鎮", "溪口鄉", "義竹鄉", "布袋鎮"); //嘉義縣 

    ZONE.SUB[14] = new Array("斗南鎮", "大埤鄉", "虎尾鎮", "土庫鎮", "褒忠鄉", "東勢鄉", "台西鄉", "崙背鄉", "麥寮鄉", "斗六市", "林內鄉", "古坑鄉", "莿桐鄉", "西螺鎮", "二崙鄉", "北港鎮", "水林鄉", "口湖鄉", "四湖鄉", "元長鄉"); //雲林縣 

    ZONE.SUB[15] = new Array("中西區", "東區", "南區", "北區", "安平區", "安南區", "永康區", "歸仁區", "新化區", "左鎮區", "玉井區", "楠西區", "南化區", "仁德區", "關廟區", "龍崎區", "官田區", "麻豆區", "佳里區", "西港區", "七股區", "將軍區", "學甲區", "北門區", "新營區", "後壁區", "白河區", "東山區", "六甲區", "下營區", "柳營區", "鹽水區", "善化區", "大內區", "山上區", "新市區", "安定區"); //台南市 

    ZONE.SUB[16] = new Array("新興區", "前金區", "苓雅區", "鹽埕區", "鼓山區", "旗津區", "前鎮區", "三民區", "楠梓區", "小港區", "左營區", "仁武區", "大社區", "岡山區", "路竹區", "阿蓮區", "田寮區", "燕巢區", "橋頭區", "梓官區", "彌陀區", "永安區", "湖內區", "鳳山區", "大寮區", "林園區", "鳥松區", "大樹區", "旗山區", "美濃區", "六龜區", "內門區", "杉林區", "甲仙區", "桃源區", "那瑪夏區", "茂林區", "茄萣區"); //高雄市 

    ZONE.SUB[17] = new Array("馬公市", "西嶼鄉", "望安鄉", "七美鄉", "白沙鄉", "湖西鄉"); //澎湖縣 

    ZONE.SUB[18] = new Array("屏東市", "三地門鄉", "霧台鄉", "瑪家鄉", "九如鄉", "里港鄉", "高樹鄉", "鹽埔鄉", "長治鄉", "麟洛鄉", "竹田鄉", "內埔鄉", "萬丹鄉", "潮州鎮", "泰武鄉", "來義鄉", "萬巒鄉", "崁頂鄉", "新埤鄉", "南州鄉", "林邊鄉", "東港鎮", "琉球鄉", "佳冬鄉", "新園鄉", "枋寮鄉", "枋山鄉", "春日鄉", "獅子鄉", "車城鄉", "牡丹鄉", "恆春鎮", "滿州鄉"); //屏東縣 

    ZONE.SUB[19] = new Array("台東市", "綠島鄉", "蘭嶼鄉", "延平鄉", "卑南鄉", "鹿野鄉", "關山鎮", "海端鄉", "池上鄉", "東河鄉", "成功鎮", "長濱鄉", "太麻里鄉", "金峰鄉", "大武鄉", "達仁鄉"); //台東縣 

    ZONE.SUB[20] = new Array("花蓮市", "新城鄉", "秀林鄉", "吉安鄉", "壽豐鄉", "鳳林鎮", "光復鄉", "豐濱鄉", "瑞穗鄉", "萬榮鄉", "玉里鎮", "卓溪鄉", "富里鄉"); //花蓮縣 

    ZONE.SUB[21] = new Array("金沙鎮", "金湖鎮", "金寧鄉", "金城鎮", "烈嶼鄉", "烏坵鄉"); //金門縣 

    ZONE.SUB[22] = new Array("南竿鄉", "北竿鄉", "莒光鄉", "東引鄉"); //連江縣 

    ZONE.CODE = new Array(23);
    ZONE.CODE[0] = new Array("0"); //請選擇 

    ZONE.CODE[1] = new Array("100", "103", "104", "105", "106", "108", "110", "111", "112", "114", "115", "116"); //台北市

    ZONE.CODE[2] = new Array("207", "208", "220", "221", "222", "223", "224", "226", "227", "228", "231", "232", "233", "234", "235", "236", "237", "238", "239", "241", "242", "243", "244", "247", "248", "249", "251", "252", "253"); //新北市

    ZONE.CODE[3] = new Array("200", "201", "202", "203", "204", "205", "206"); //基隆市

    ZONE.CODE[4] = new Array("260", "261", "262", "263", "264", "265", "266", "267", "268", "269", "270", "272", "290"); //宜蘭縣

    ZONE.CODE[5] = new Array("320", "324", "325", "326", "327", "328", "330", "333", "334", "335", "336", "337", "338"); //桃園市

    ZONE.CODE[6] = new Array("300", "300", "300"); //新竹市

    ZONE.CODE[7] = new Array("302", "303", "304", "305", "306", "307", "308", "310", "311", "312", "313", "314", "315"); //新竹縣

    ZONE.CODE[8] = new Array("350", "351", "352", "353", "354", "356", "357", "358", "360", "361", "362", "363", "364", "365", "366", "367", "368", "369"); //苗栗縣

    ZONE.CODE[9] = new Array("400", "401", "402", "403", "404", "406", "407", "408", "411", "412", "413", "414", "420", "421", "422", "423", "424", "426", "427", "428", "429", "432", "433", "434", "435", "436", "437", "438", "439"); //台中市

    ZONE.CODE[10] = new Array("500", "502", "503", "504", "505", "506", "507", "508", "509", "510", "511", "512", "513", "514", "515", "516", "520", "521", "522", "523", "524", "525", "526", "527", "528", "530"); //彰化縣

    ZONE.CODE[11] = new Array("540", "541", "542", "544", "545", "546", "551", "552", "553", "555", "556", "557", "558"); //南投縣

    ZONE.CODE[12] = new Array("600", "600"); //嘉義市

    ZONE.CODE[13] = new Array("602", "603", "604", "605", "606", "607", "608", "611", "612", "613", "614", "615", "616", "621", "622", "623", "624", "625"); //嘉義縣

    ZONE.CODE[14] = new Array("630", "631", "632", "633", "634", "635", "636", "637", "638", "640", "643", "646", "647", "648", "649", "651", "652", "653", "654", "655"); //雲林縣

    ZONE.CODE[15] = new Array("700", "701", "702", "704", "708", "709", "710", "711", "712", "713", "714", "715", "716", "717", "718", "719", "720", "721", "722", "723", "724", "725", "726", "727", "730", "731", "732", "733", "734", "735", "736", "737", "741", "742", "743", "744", "745"); //台南市

    ZONE.CODE[16] = new Array("800", "801", "802", "803", "804", "805", "806", "807", "811", "812", "813", "814", "815", "820", "821", "822", "823", "824", "825", "826", "827", "828", "829", "830", "831", "832", "833", "840", "842", "843", "844", "845", "846", "847", "848", "849", "851", "852"); //高雄市

    ZONE.CODE[17] = new Array("880", "881", "882", "883", "884", "885"); //澎湖縣

    ZONE.CODE[18] = new Array("900", "901", "902", "903", "904", "905", "906", "907", "908", "909", "911", "912", "913", "920", "921", "922", "923", "924", "925", "926", "927", "928", "929", "931", "932", "940", "941", "942", "943", "944", "945", "946", "947"); //屏東縣

    ZONE.CODE[19] = new Array("950", "951", "952", "953", "954", "955", "956", "957", "958", "959", "961", "962", "963", "964", "965", "966"); //台東縣

    ZONE.CODE[20] = new Array("970", "971", "972", "973", "974", "975", "976", "977", "978", "979", "981", "982", "983"); //花蓮縣

    ZONE.CODE[21] = new Array("890", "891", "892", "893", "894", "896"); //金門縣

    ZONE.CODE[22] = new Array("209", "210", "211", "212"); //連江縣
    // MAIN

    $.each(ZONE.MAIN, function (KEY, VALUE) {
      var op_val = VALUE == "請選擇" ? "" : VALUE;
      ZONE.MAIN_STR += '<option value="' + op_val + '" rel="' + KEY + '">' + VALUE + '</option>';
    });
    $("#" + ZONE.MAIN_ID).append(ZONE.MAIN_STR); // SUB

    $("#" + ZONE.MAIN_ID).change(function () {
      var MAIN_KEY = $(this).find("option:selected").attr('rel');
      zip_row(MAIN_KEY);
      $("#" + ZONE.SUB_ID).html("").append(ZONE.SUB_STR);
      zip_load();
    });
    $("#" + ZONE.SUB_ID).change(function () {
      zip_load();
    }); // ZIP RE SELECT

    if (ZONE.ZIP_GET != "") {
      for (var I = 0; I < ZONE.SUB.length; I++) {
        zip_row(I);
      }
    } // ZIP INPUT


    $("#" + ZONE.ZIP_ID).keyup(function () {
      ZONE.ZIP_GET = $(this).val();

      for (var I = 0; I < ZONE.SUB.length; I++) {
        zip_row(I);
      }
    }); // ZIP

    function zip_load() {
      var ZIP = $("#" + ZONE.SUB_ID).find("option:selected").attr('zip');
      $("#" + ZONE.ZIP_ID).html("").val(ZIP);

      try {
        CALLBACK(ZIP);
      } catch (e) {}
    } // ZIP_ROW


    function zip_row(MAIN_KEY) {
      ZONE.SUB_STR = "";
      $.each(ZONE.SUB[MAIN_KEY], function (KEY, VALUE) {
        var KEY_FIRST = KEY == 0 && ZONE.ZIP_GET == "" || ZONE.ZIP_GET != "" && ZONE.ZIP_GET == ZONE.CODE[MAIN_KEY][KEY] ? 'selected' : '';
        if (!ZONE.RESELECT && ZONE.DEFAULT_AREA != "") var KEY_FIRST = ZONE.DEFAULT_AREA == VALUE ? 'selected' : '';
        ZONE.SUB_STR += '<option value="' + VALUE + '" ' + KEY_FIRST + ' zip="' + ZONE.CODE[MAIN_KEY][KEY] + '">' + VALUE + '</option>';

        if (ZONE.ZIP_GET == ZONE.CODE[MAIN_KEY][KEY]) {
          $("#" + ZONE.MAIN_ID + " option:eq(" + MAIN_KEY + ")").attr("selected", "selected");
          ZONE.REINPUT = true;
        }
      });

      if (ZONE.REINPUT) {
        $("#" + ZONE.SUB_ID).html("").append(ZONE.SUB_STR);
        ZONE.REINPUT = false;
        ZONE.RESELECT = true;
        zip_load();
      }
    }
  };
})(jQuery);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "../node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./js/page/cart2.js":
/*!**************************!*\
  !*** ./js/page/cart2.js ***!
  \**************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! js-cookie */ "../node_modules/js-cookie/src/js.cookie.js");
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(js_cookie__WEBPACK_IMPORTED_MODULE_0__);


__webpack_require__(/*! ../components/flatpickr */ "./js/components/flatpickr.js");

__webpack_require__(/*! ../components/validate */ "./js/components/validate.js");

__webpack_require__(/*! ../components/zone_box */ "./js/components/zone_box.js");

$(document).ready(function () {
  $($('[name=order_zip]').parent().parent()).zone_box({
    MAIN_ID: "order_country",
    SUB_ID: "order_district",
    ZIP_ID: "order_zip",
    ZIP_GET: "",
    DEFAULT_AREA: ""
  });
  $($('[name=reci_zip]').parent().parent()).zone_box({
    MAIN_ID: "reci_country",
    SUB_ID: "reci_district",
    ZIP_ID: "reci_zip",
    ZIP_GET: "",
    DEFAULT_AREA: ""
  });
  $('[name=order_zip]').trigger('keyup');
  $('[name=reci_zip]').trigger('keyup');
});
var form_o = js_cookie__WEBPACK_IMPORTED_MODULE_0___default.a.get('order_form') ? js_cookie__WEBPACK_IMPORTED_MODULE_0___default.a.get('order_form') : null;

if (form_o) {
  form_o = JSON.parse(form_o);
  Object.keys(form_o).map(function (e, i) {
    var a = $("[name=\"".concat(e, "\"]"));

    if (a.length >= 0) {
      if (!form_o[e] || form_o[e].length <= 0) {
        return;
      } else if (e.includes('addresses')) {
        var prefix = e.split('_')[0];
        $("[name=\"".concat(prefix, "_country\"]")).val(form_o[e]['country']);
        $("[name=\"".concat(prefix, "_district\"]")).val(form_o[e]['district']);
        $("[name=\"".concat(prefix, "_address\"]")).val(form_o[e]['address']);
        $("[name=\"".concat(prefix, "_zip\"]")).val(form_o[e]['zip']);
      } else {
        var type = a.prop('type');

        if (type == 'radio') {
          a.each(function (i, el) {
            if ($(el).val() == form_o[e]['value']) {
              $(el).prop('checked', 'checked');

              if (e.includes('invoice_type')) {
                $(el).parent().find('[name*="ein-"]').val(form_o[e + '_info']['value']);
              }
            }
          });
        } else {
          a.val(form_o[e]['value']);
        }
      }
    }
  });
  SELFORDELIVERY($('input[name="shipment_type"]'));
}

$('input[name="shipment_type"]').on('change', function () {
  SELFORDELIVERY(this);
});
$('input[name="same_orderer"]').on('change', function (e) {
  if ($(this).prop('checked')) {
    var a = $('.form [name*="order_"]');
    var names = {};
    a.each(function (i, e) {
      var name = $(e).prop('name').split('_')[1];
      var val = $(e).val();
      names[name] = val;
    });
    Object.keys(names).map(function (el, index) {
      if (el == 'zip') {
        $(".form [name=\"reci_".concat(el, "\"]")).val(names[el]);
        $(".form [name=\"reci_".concat(el, "\"]")).trigger('keyup');
        return;
      }

      $(".form [name=\"reci_".concat(el, "\"]")).val(names[el]);
    });
  }
});
$('.cart_submit').on('click', function (e) {
  var form = {
    payment_type: {
      text: $('[name="payment_type"]:checked').parent().find('label').text(),
      value: $('[name="payment_type"]').val()
    },
    shipment_type: {
      text: $('[name="shipment_type"]:checked').parent().find('label').text(),
      value: $('[name="shipment_type"]').val()
    },
    address: {
      text: $('[name="address"]').prop('placeholder'),
      value: $('[name="address"]').prop('placeholder')
    },
    pickup_date: {
      text: $('[name="pickup_date"]').val(),
      value: $('[name="pickup_date"]').val()
    },
    order_name: {
      text: $('[name="order_name"]').val(),
      value: $('[name="order_name"]').val()
    },
    order_phone: {
      text: $('[name="order_phone"]').val(),
      value: $('[name="order_phone"]').val()
    },
    order_addresses: {
      country: $('[name="order_country"]').val(),
      district: $('[name="order_district"]').val(),
      address: $('[name="order_address"]').val(),
      zip: $('[name="order_zip"]').val(),
      addresses: $('[name="order_country"]').val() + $('[name="order_district"]').val() + $('[name="order_address"]').val() + ' ' + $('[name="order_zip"]').val()
    },
    order_email: {
      text: $('[name="order_email"]').val(),
      value: $('[name="order_email"]').val()
    },
    order_comment: {
      text: $('[name="order_comment"]').val(),
      value: $('[name="order_comment"]').val()
    },
    reci_name: {
      text: $('[name="reci_name"]').val(),
      value: $('[name="reci_name"]').val()
    },
    reci_phone: {
      text: $('[name="reci_phone"]').val(),
      value: $('[name="reci_phone"]').val()
    },
    reci_addresses: {
      country: $('[name="reci_country"]').val(),
      district: $('[name="reci_district"]').val(),
      address: $('[name="reci_address"]').val(),
      zip: $('[name="reci_zip"]').val(),
      addresses: $('[name="reci_country"]').val() + $('[name="reci_district"]').val() + $('[name="reci_address"]').val() + ' ' + $('[name="reci_zip"]').val()
    },
    reci_email: {
      text: $('[name="reci_email"]').val(),
      value: $('[name="reci_email"]').val()
    },
    reci_comment: {
      text: $('[name="reci_comment"]').val(),
      value: $('[name="reci_comment"]').val()
    },
    invoice_type: {
      text: $('[name="invoice_type"]:checked').parent().find('label').text(),
      value: $('[name="invoice_type"]:checked').val()
    },
    invoice_type_info: {
      text: $('[name="invoice_type"]:checked').parent().find('[name*="ein-"]').val(),
      value: $('[name="invoice_type"]:checked').parent().find('[name*="ein-"]').val(),
      name: $('[name="invoice_type"]:checked').parent().find('[name*="ein-"]').prop('name')
    }
  };
  js_cookie__WEBPACK_IMPORTED_MODULE_0___default.a.set('order_form', form, {
    expires: 1
  });
});

function SELFORDELIVERY(e) {
  var target = $(e).parent().parent().parent().parent().next();
  var deliverymax = $(e).data('deliverymax');
  var deliveryfee = $(e).data('deliveryfee');

  if ($(e).val() == 'DELIVERY') {
    $(target).find('.col').eq(0).hide();
    $(target).find('.col').eq(1).addClass('full');

    if ($('.delivery_fee').length) {
      var total_price = parseInt($('.cart_details li:last-child span:last-child').text().replace('NT$ ', ''));

      if (total_price <= deliverymax) {
        $('.delivery_fee').remove();
        total_price = total_price - deliveryfee;
        $('.cart_details li:last-child span:last-child').text(total_price);
      }
    }
  } else {
    $(target).find('.col').eq(0).show();
    $(target).find('.col').eq(1).removeClass('full');

    if (!$('.delivery_fee').length) {
      var _total_price = parseInt($('.cart_details li:last-child span:last-child').text().replace('NT$ ', ''));

      if (_total_price <= deliverymax) {
        var template = "<li class=\"delivery_fee\"><span>\u904B\u8CBB\uFF1A</span><span>$ 130</span></li>";
        $(template).insertAfter($('.cart_details li:nth-child(1)'));
        _total_price = _total_price + deliveryfee;
        $('.cart_details li:last-child span:last-child').text(_total_price);
      }
    }
  }
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "../node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ 6:
/*!***************************!*\
  !*** multi page/cart2.js ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! page/cart2.js */"./js/page/cart2.js");


/***/ })

/******/ });
//# sourceMappingURL=cart2.js.map