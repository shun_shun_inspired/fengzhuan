/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(Object.prototype.hasOwnProperty.call(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"product": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push([2,"vendor"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js/components/swiper.js":
/*!*********************************!*\
  !*** ./js/components/swiper.js ***!
  \*********************************/
/*! exports provided: buildSwiper */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "buildSwiper", function() { return buildSwiper; });
/* harmony import */ var _node_modules_swiper_dist_js_swiper__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../node_modules/swiper/dist/js/swiper */ "../node_modules/swiper/dist/js/swiper.js");
/* harmony import */ var _node_modules_swiper_dist_js_swiper__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_swiper_dist_js_swiper__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_swiper_dist_css_swiper_min_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../node_modules/swiper/dist/css/swiper.min.css */ "../node_modules/swiper/dist/css/swiper.min.css");
/* harmony import */ var _node_modules_swiper_dist_css_swiper_min_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_swiper_dist_css_swiper_min_css__WEBPACK_IMPORTED_MODULE_1__);
 // import Swiper styles


function buildSwiper(ele, options) {
  return new _node_modules_swiper_dist_js_swiper__WEBPACK_IMPORTED_MODULE_0___default.a(ele, options);
}

/***/ }),

/***/ "./js/function/func.js":
/*!*****************************!*\
  !*** ./js/function/func.js ***!
  \*****************************/
/*! exports provided: hiddenHeaderAndFootrt, backToprev, spinButton, countItemPrice, countTotalSubPrice, countDiscountPrice, countTotalPrice, cancelOrder, changeProductImage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "hiddenHeaderAndFootrt", function() { return hiddenHeaderAndFootrt; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "backToprev", function() { return backToprev; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "spinButton", function() { return spinButton; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "countItemPrice", function() { return countItemPrice; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "countTotalSubPrice", function() { return countTotalSubPrice; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "countDiscountPrice", function() { return countDiscountPrice; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "countTotalPrice", function() { return countTotalPrice; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "cancelOrder", function() { return cancelOrder; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "changeProductImage", function() { return changeProductImage; });
function hiddenHeaderAndFootrt() {
  $(document).ready(function () {
    $('.header,.footer').addClass('none');
    $('.main').addClass('none');
  });
}
function backToprev() {
  $(document).ready(function () {
    $('.back.backToprev').on('click touch', function (e) {
      e.preventDefault();
      window.location.href = "index.html";
    });
  });
}
function spinButton() {
  $('.spinButton .plus,.spinButton .less').on('click touch', function () {
    var qty = parseInt($(this).attr('value'));
    var current_qty = parseInt($(this).parent().find('input[type="number"]').val());
    var res = parseInt($(this).parent().find('input[type="number"]').val());

    if (current_qty <= 1 && qty == -1) {
      res = 1;
    } else {
      res += qty;
    }

    $(this).parent().find('input[type="number"]').val(res);
    countItemPrice(this, res);
  });
}
function countItemPrice(element, qty) {
  var origin_price = $(element).parent().parent().parent().find('.origin_price').text();
  var item_total_price = parseInt(origin_price) * parseInt(qty);
  var item_total = $(element).parent().parent().parent().find('.item_total');
  item_total.text(item_total_price);
  countTotalSubPrice();
}
function countTotalSubPrice() {
  var totalPrice = 0;
  $('.cart-table_td').each(function (index, element) {
    var item_price = parseInt($(element).find('.item_total').text().trim());
    totalPrice += item_price;
  });
  $('.order_sub_total_price').text(totalPrice);
  countDiscountPrice();
}
function countDiscountPrice() {
  var discount = parseInt($('span.order_discount_price').text());
  var subTotal = parseInt($('.order_sub_total_price').text());
  var ratio = Math.floor(subTotal / discount);
  var qty = 0;
  $('.cart-table_td').each(function (index, element) {
    qty += parseInt($(element).find('bdi').text().trim());
  });
  $('span.order_discount_price').text(qty * ratio);
  countTotalPrice();
}
function countTotalPrice() {
  var sub_price = $('.order_sub_total_price').text();
  var discount_price = $('.order_discount_price').text();
  var total_price = $('.order_total_price');
  total_price.text(parseInt(sub_price) - parseInt(discount_price));
}
function cancelOrder(container) {
  $('.cart-table_cancel,.cart-table_cancel-mb').on('click touch', function () {
    var parent = $(this).parent().parent();
    $(parent).remove();
    countTotalSubPrice();
  });
} //  改變產品圖片(20210908小丘)

function changeProductImage() {
  // $('product-card_dropdown__title')
  $("input[name='spec_id']").on("click touch", function () {
    if ($(this).attr('data-img') === "") {
      return false;
    }

    $(".product-card_image__bg > div").css({
      'backgroundImage': "url('".concat($(this).attr('data-img'), "')")
    });
  });
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "../node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./js/page/product.js":
/*!****************************!*\
  !*** ./js/page/product.js ***!
  \****************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var _components_swiper__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../components/swiper */ "./js/components/swiper.js");
/* harmony import */ var _function_func__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../function/func */ "./js/function/func.js");


Object(_components_swiper__WEBPACK_IMPORTED_MODULE_0__["buildSwiper"])('.product_content__other_swiper__container', {
  slidesPerView: 3,
  spaceBetween: '7%',
  navigation: {
    nextEl: '.product_content__other_swiper__next',
    prevEl: '.product_content__other_swiper__prev',
    hideOnClick: true,
    disabledClass: 'swiper-navigation-disabled'
  },
  breakpoints: {
    768: {
      slidesPerView: 'auto',
      spaceBetween: 30,
      direction: 'vertical',
      allowTouchMove: false
    }
  }
});
Object(_components_swiper__WEBPACK_IMPORTED_MODULE_0__["buildSwiper"])('.product_content__swiper_container', {
  slidesPerView: 3,
  spaceBetween: '2%',
  navigation: {
    nextEl: '.product_content__swiper_next',
    prevEl: '.product_content__swiper_prev',
    hideOnClick: true,
    disabledClass: 'swiper-navigation-disabled'
  },
  breakpoints: {
    768: {
      slidesPerView: 'auto',
      spaceBetween: 30,
      direction: 'vertical',
      allowTouchMove: false
    }
  }
});
var meun = ['產品須知', '退換貨須知'];
Object(_components_swiper__WEBPACK_IMPORTED_MODULE_0__["buildSwiper"])('.product_content__note_list', {
  slidesPerView: 1,
  spaceBetween: 0,
  autoHeight: true,
  pagination: {
    el: '.product_content__note_pagination',
    clickable: true,
    renderBullet: function renderBullet(index, className) {
      return "<div class=\"product_content__note_title ".concat(className, "\">\n                        <svg width=\"26\" height=\"25\" viewBox=\"0 0 26 25\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n                            <path class=\"icon\" d=\"M23 5.5H20.275C20.4125 5.1125 20.5 4.6875 20.5 4.25C20.5 2.175 18.825 0.5 16.75 0.5C15.4375 0.5 14.3 1.175 13.625 2.1875L13 3.025L12.375 2.175C11.7 1.175 10.5625 0.5 9.25 0.5C7.175 0.5 5.5 2.175 5.5 4.25C5.5 4.6875 5.5875 5.1125 5.725 5.5H3C1.6125 5.5 0.5125 6.6125 0.5125 8L0.5 21.75C0.5 23.1375 1.6125 24.25 3 24.25H23C24.3875 24.25 25.5 23.1375 25.5 21.75V8C25.5 6.6125 24.3875 5.5 23 5.5ZM16.75 3C17.4375 3 18 3.5625 18 4.25C18 4.9375 17.4375 5.5 16.75 5.5C16.0625 5.5 15.5 4.9375 15.5 4.25C15.5 3.5625 16.0625 3 16.75 3ZM9.25 3C9.9375 3 10.5 3.5625 10.5 4.25C10.5 4.9375 9.9375 5.5 9.25 5.5C8.5625 5.5 8 4.9375 8 4.25C8 3.5625 8.5625 3 9.25 3ZM23 21.75H3V19.25H23V21.75ZM23 15.5H3V8H9.35L6.75 11.5375L8.775 13L11.75 8.95L13 7.25L14.25 8.95L17.225 13L19.25 11.5375L16.65 8H23V15.5Z\" fill=\"#C9AB81\"/>\n                        </svg>\n                        <p>").concat(meun[index], "</p>\n                    </div>");
    }
  }
});
$(document).ready(function () {
  $('input[name="spec_id"]').on('change', function () {
    var price = $(this).data('price');
    var qty = $('.product_content__top_right__add-spinButton input[type="number"]').val();
    var res_price = parseInt(qty) * parseInt(price);
    $('.product-card_link__left.product_content__top_right__add-card_link__left').text("$ ".concat(res_price));
    $('.product-card_link__left.product_content__top_right__add-card_link__left').attr('data-originprice', price);
  });
  $('.product_content__top_right__add-spinButton button').on('click', function () {
    var _this = this;

    var target = $('.product-card_link__left.product_content__top_right__add-card_link__left');
    var o_price = target.attr('data-originprice');
    var qty = 1,
        total = 0;
    setTimeout(function () {
      qty = $(_this).parent().find('input[type="number"]').val();
      total = parseInt(o_price) * parseInt(qty);
      target.text("$ ".concat(total));
    }, 100);
  });
});
Object(_function_func__WEBPACK_IMPORTED_MODULE_1__["spinButton"])();
Object(_function_func__WEBPACK_IMPORTED_MODULE_1__["changeProductImage"])();
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "../node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ 2:
/*!*****************************!*\
  !*** multi page/product.js ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! page/product.js */"./js/page/product.js");


/***/ })

/******/ });
//# sourceMappingURL=product.js.map