/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(Object.prototype.hasOwnProperty.call(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"products": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push([3,"vendor"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js/function/func.js":
/*!*****************************!*\
  !*** ./js/function/func.js ***!
  \*****************************/
/*! exports provided: hiddenHeaderAndFootrt, backToprev, spinButton, countItemPrice, countTotalSubPrice, countDiscountPrice, countTotalPrice, cancelOrder, changeProductImage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "hiddenHeaderAndFootrt", function() { return hiddenHeaderAndFootrt; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "backToprev", function() { return backToprev; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "spinButton", function() { return spinButton; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "countItemPrice", function() { return countItemPrice; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "countTotalSubPrice", function() { return countTotalSubPrice; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "countDiscountPrice", function() { return countDiscountPrice; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "countTotalPrice", function() { return countTotalPrice; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "cancelOrder", function() { return cancelOrder; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "changeProductImage", function() { return changeProductImage; });
function hiddenHeaderAndFootrt() {
  $(document).ready(function () {
    $('.header,.footer').addClass('none');
    $('.main').addClass('none');
  });
}
function backToprev() {
  $(document).ready(function () {
    $('.back.backToprev').on('click touch', function (e) {
      e.preventDefault();
      window.location.href = "index.html";
    });
  });
}
function spinButton() {
  $('.spinButton .plus,.spinButton .less').on('click touch', function () {
    var qty = parseInt($(this).attr('value'));
    var current_qty = parseInt($(this).parent().find('input[type="number"]').val());
    var res = parseInt($(this).parent().find('input[type="number"]').val());

    if (current_qty <= 1 && qty == -1) {
      res = 1;
    } else {
      res += qty;
    }

    $(this).parent().find('input[type="number"]').val(res);
    countItemPrice(this, res);
  });
}
function countItemPrice(element, qty) {
  var origin_price = $(element).parent().parent().parent().find('.origin_price').text();
  var item_total_price = parseInt(origin_price) * parseInt(qty);
  var item_total = $(element).parent().parent().parent().find('.item_total');
  item_total.text(item_total_price);
  countTotalSubPrice();
}
function countTotalSubPrice() {
  var totalPrice = 0;
  $('.cart-table_td').each(function (index, element) {
    var item_price = parseInt($(element).find('.item_total').text().trim());
    totalPrice += item_price;
  });
  $('.order_sub_total_price').text(totalPrice);
  countDiscountPrice();
}
function countDiscountPrice() {
  var discount = parseInt($('span.order_discount_price').text());
  var subTotal = parseInt($('.order_sub_total_price').text());
  var ratio = Math.floor(subTotal / discount);
  var qty = 0;
  $('.cart-table_td').each(function (index, element) {
    qty += parseInt($(element).find('bdi').text().trim());
  });
  $('span.order_discount_price').text(qty * ratio);
  countTotalPrice();
}
function countTotalPrice() {
  var sub_price = $('.order_sub_total_price').text();
  var discount_price = $('.order_discount_price').text();
  var total_price = $('.order_total_price');
  total_price.text(parseInt(sub_price) - parseInt(discount_price));
}
function cancelOrder(container) {
  $('.cart-table_cancel,.cart-table_cancel-mb').on('click touch', function () {
    var parent = $(this).parent().parent();
    $(parent).remove();
    countTotalSubPrice();
  });
} //  改變產品圖片(20210908小丘)

function changeProductImage() {
  // $('product-card_dropdown__title')
  $("input[name='spec_id']").on("click touch", function () {
    if ($(this).attr('data-img') === "") {
      return false;
    }

    $(".product-card_image__bg > div").css({
      'backgroundImage': "url('".concat($(this).attr('data-img'), "')")
    });
  });
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "../node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./js/page/products.js":
/*!*****************************!*\
  !*** ./js/page/products.js ***!
  \*****************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var _function_func__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../function/func */ "./js/function/func.js");

$(document).ready(function () {
  $('.products_dropdown__toggle').on('click touch', function () {
    var target = $(this).next();

    if (!target.hasClass('active')) {
      $('.products_dropdown__meun').removeClass('active');
      target.addClass('active');
    } else {
      target.removeClass('active');
    }
  });
  $('body').on('click touch', function (event) {
    if (!$(event.target).closest('.products_dropdown__toggle').length && !$(event.target).is('.products_dropdown__toggle')) {
      $('.products_dropdown__meun').removeClass('active');
    }
  });
});
$(document).ready(function () {
  var qty = $('.products_dropdown-mb li').length;
  var duration = Math.min(36.36 * parseInt(qty) * 1.5, 400);
  $('.products_dropdown-mb_title').on('click', function () {
    var _this = this;

    if (!$(this).hasClass('active')) {
      $(this).addClass('active');
      $(this).next().addClass('active').animate({
        maxHeight: '118.93333vw',
        overflow: 'hidden',
        paddingTop: 0,
        paddingBottom: 0,
        marginTop: 0,
        marginBottom: 0
      }, duration);
    } else {
      $(this).removeClass('active');
      $(this).next().animate({
        maxHeight: '0px',
        overflow: 'hidden',
        paddingTop: 0,
        paddingBottom: 0,
        marginTop: 0,
        marginBottom: 0
      }, duration);
      setTimeout(function () {
        $(_this).next().removeClass('active');
      }, duration);
    }
  });
  $('.products_dropdown-mb li').on('click', function () {
    window.location.href = $(this).find('a').attr('href');
    localStorage.setItem('products_dropdown-mb', [$(this).text(), $(this).data('index')]);
  });
});
$(document).ready(function () {
  var current_cate = localStorage.getItem('products_dropdown-mb') ? localStorage.getItem('products_dropdown-mb').split(',') : [];

  if (current_cate) {
    $('.products_dropdown-mb_title').text(current_cate[0]);
    $('.products_dropdown-mb li').removeClass('active');
    $('.products_dropdown-mb li[data-index=' + current_cate[1] + ']').addClass('active');
  }
});
$(document).ready(function () {
  $('.product-card_dropdown__title').on('click', function (e) {
    var select = $(this).next();

    if (!select.hasClass('active')) {
      $('.product-card_dropdown__select').removeClass('active');
      select.addClass('active');
    } else {
      select.removeClass('active');
    }

    $('body').on('click touch', function (event) {
      if (!$(event.target).closest('.product-card_dropdown').length && !$(event.target).is('.product-card_dropdown') && !$(event.target).closest('.product-card_dropdown__title').length && !$(event.target).is('.product-card_dropdown__title')) {
        $('.product-card_dropdown__select').removeClass('active');
      }
    });
  });
  $('.product-card_dropdown__select li').on('click', function () {
    var _this_val = $(this).text();

    var _this_price = $(this).data('price');

    var target_parent_parent = $(this).parent().parent().parent();
    var qty = target_parent_parent.find('bdi');
    var target_parent = $(this).parent().parent();
    var target = $(this).parent();
    var target_title = $(this).parent().prev();
    target.removeClass('active');
    target_parent.data('value', _this_val);
    target_title.text(_this_val);
    target.find('li').removeClass('active');
    $(this).addClass('active');
    target_parent_parent.find('.product-card_link__left').attr('data-originprice', _this_price).text("$ ".concat(_this_price));
    qty.text(1);
  });
});
$(document).ready(function () {
  $('.products_item_card-spinButton button').on('click', function () {
    var _this2 = this;

    var o_price_target = $(this).parent().parent().find('.product-card_link__left');
    var o_price = o_price_target.attr('data-originprice');
    var qty = 0,
        t_price = 0;
    setTimeout(function () {
      qty = $(_this2).parent().find('bdi').text();
      t_price = parseInt(qty) * parseInt(o_price);
      o_price_target.text("$ ".concat(t_price));
    }, 100);
  });
});
Object(_function_func__WEBPACK_IMPORTED_MODULE_0__["spinButton"])();
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "../node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ 3:
/*!******************************!*\
  !*** multi page/products.js ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! page/products.js */"./js/page/products.js");


/***/ })

/******/ });
//# sourceMappingURL=products.js.map