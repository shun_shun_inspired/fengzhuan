var path = require( "path" );
var webpack = require( 'webpack' );
const MiniCssExtractPlugin = require( "mini-css-extract-plugin" );
const CopyWebpackPlugin = require( 'copy-webpack-plugin' );
const HtmlWebpackPlugin = require( 'html-webpack-plugin' )
const glob = require( 'glob' );
var now = new Date().getTime()

const config = {
    mode: process.env.NODE_ENV,
    // 指定所有entry的資料夾
    context: path.resolve( __dirname, "src" ),
    // main: 'main.js', 以下為extensions所產生縮寫
    entry: {
        main: [ 'main.js', 'main.scss' ],
        index: [ 'page/index.js' ],
        product: [ 'page/product.js' ],
        products: [ 'page/products.js' ],
        log: [ 'page/log.js' ],
        cart: [ 'page/cart.js' ],
        cart2: [ 'page/cart2.js' ],
        cart4: [ 'page/cart4.js' ],
        account: [ 'page/account.js' ],
        contact: [ 'page/contact.js' ],
        fqa: [ 'page/fqa.js' ],
    },
    node: {
        fs: 'empty',
        net: 'empty',
        dns: 'empty',
        tls: 'empty',
    },
    output: {
        // 依照entry的name修正output檔名
        path: path.resolve( __dirname, "dist" ),
        filename: 'js/[name].js'
    },
    devtool: 'source-map',
    optimization: {
        // 將引入的套件所需的程式碼提取出來變成一個不會開啟的但又必備的JS檔或是將公用的function提取出來變成一包專用的公用檔案
        splitChunks: {
            cacheGroups: {
                vendor: {
                    test: /node_modules/,
                    name: 'vendor', //分割出來的檔案命名
                    chunks: 'initial', //把非動態模塊打包，動態模塊進行優化打包
                    enforce: true
                }
            }
        },
        minimize: false

    },
    devServer: {
        compress: true,
        port: 3001,
        stats: {
            assets: true,
            cached: false,
            chunkModules: false,
            chunkOrigins: false,
            chunks: false,
            colors: true,
            hash: false,
            modules: false,
            reasons: false,
            source: false,
            version: false,
            warnings: false
        }
    },
    plugins: [
        // 全域，不需要在各個entry獨立引入jquery
        // 建議少用providePlugin
        new webpack.ProvidePlugin( {
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery'
        } ),

        // filename: output
        new MiniCssExtractPlugin( {
            filename: 'css/[name].css'
        } ),

        // 原封不動copy
        new CopyWebpackPlugin( [ {
            from: 'assets',
            to: 'assets'
        } ] ),
        new CopyWebpackPlugin( [ {
            from: 'fonts',
            to: 'fonts'
        } ] )
    ],
    resolve: {
        // entry 可以省略路徑
        // 告訴webpack 解析模塊時應該搜索的目錄。
        // 絕對路徑和相對路徑都能使用
        modules: [
            path.resolve( 'src' ),
            path.resolve( 'src/js' ),
            path.resolve( 'src/scss' ),
            path.resolve( 'src/assets' ),
            path.resolve( 'src/fonts' ),
            path.resolve( 'src/libs' ),
            path.resolve( 'node_modules' )
        ],
        // 創建import或require的別名，來確保模塊引入變得更簡單
        // 範例 ： "TweenLite": path.resolve( 'node_modules', 'gsap/src/minified/TweenLite.min.js' ),
        alias: {
            // "ScrollMagic": path.resolve( 'node_modules', 'scrollmagic/scrollmagic/uncompressed/ScrollMagic.js' ),
            // "animation.gsap": path.resolve( 'node_modules', 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap.js' ),
            // "debug.addIndicators": path.resolve( 'node_modules', 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators.js' )
        },
        // entry 可以省略副檔名
        extensions: [ '.js', '.scss', '.css' ]
    },
    module: {
        rules: [
            {
                test: /\.pug$/,
                use: [
                    {
                        loader: 'html-loader',
                        options: {
                            minimize: false
                        }
                    },
                    {
                        loader: 'pug-html-loader',
                        options: {
                            pretty: true
                        }
                    },
                ]
            },
            {
                test: /\.css$/,
                exclude: /libs/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            publicPath: "../"
                        }
                    },
                    'css-loader',
                    `postcss-loader`
                ]
            },
            {
                test: /\.(sa|sc)ss$/,
                exclude: /libs/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            publicPath: "../",
                            sourceMap: true,
                            sourceMapContents: true
                        }
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            // importLoaders: 1,
                            // modules: true,
                            // localIdentName: '[path][name]__[local]--[hash:base64:5]'
                        }
                    },
                    `postcss-loader`,
                    'sass-loader',
                ]
            },
            {
                test: /\.js$/,
                exclude: /bower_components/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [ '@babel/preset-env' ]
                    }
                }
            },
            {
                test: /\.(jpe?g|png|gif)$/,
                use: [ {
                    loader: 'file-loader',
                    options: {
                        name: '[path][name].[ext]',
                        publicPath: '../'
                    }
                } ]
            },
            {
                // sass等有引入assets檔案時，需透過『file-loader』判別副檔案
                test: /\.(woff|woff2|ttf|eot)$/,
                loader: 'file-loader',
                options: {
                    name: '[path][name].[ext]',
                    outputPath: 'fonts/'
                }
            },
            {
                test: /\.svg$/,
                exclude: /upload/,
                use: [ {
                    loader: 'file-loader',
                    options: {
                        name: '[path][name].[ext]',
                        publicPath: '../'
                    }
                } ]
            }
        ]
    }
}

glob.sync( 'src/pug/view/*.pug' ).forEach( ( name ) =>
{
    const start = name.indexOf( '/pug/view/' ) + 5;
    const end = name.length - 4;
    const n = name.slice( start, end );
    let chunks = []

    //- console.log( n );

    switch ( n )
    {
        case 'view/index':
            chunks = [ 'main', 'index' ]
            break;
        case 'view/product':
            chunks = [ 'main', 'product' ]
            break;
        case 'view/products':
            chunks = [ 'main', 'products' ]
            break;
        case 'view/login':
            chunks = [ 'main', 'log' ]
            break;
        case 'view/signup':
            chunks = [ 'main', 'log' ]
            break;
        case 'view/pwd-reset':
            chunks = [ 'main', 'log' ]
            break;
        case 'view/cart':
            chunks = [ 'main', 'cart' ]
            break;
        case 'view/cart2':
            chunks = [ 'main', 'cart2' ]
            break;
        case 'view/cart4':
            chunks = [ 'main', 'cart4' ]
            break;
        case 'view/account':
            chunks = [ 'main', 'account' ]
            break;
        case 'view/order':
            chunks = [ 'main', 'account' ]
            break;
        case 'view/order_detail':
            chunks = [ 'main', 'account' ]
            break;
        case 'view/contact':
            chunks = [ 'main', 'contact' ]
            break;
        case 'view/FQA':
            chunks = [ 'main', 'fqa' ]
            break;
        default:
            chunks = [ 'main' ]
            break;
    }

    config.plugins.push(
        new HtmlWebpackPlugin( {
            template: path.join( __dirname, 'src/pug/' + n + '.pug' ),
            filename: n + '.html',
            chunks: chunks,
            hash: true,
            // excludeChunks:[],
            inject: true,
            minify: {
                sortAttributes: false,
                collapseWhitespace: false,
                collapseBooleanAttributes: false,
                removeComments: false
            }
        } )
    );
} );


module.exports = config;