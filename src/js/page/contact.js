import mapboxgl from '!mapbox-gl';

import $ from 'jquery'
import validate from 'jquery-validation'

let point = [ 24.14620815716892, 120.65493218409327 ].reverse()

mapboxgl.accessToken = 'pk.eyJ1Ijoib2djaW5nNzgiLCJhIjoiY2ttOTVmdGM3MHpkZzJ3bGEyZjFjZGw3ZSJ9.blAiOhDLRIZjslgtpebfgQ';
var map = new mapboxgl.Map( {
    container: 'map',
    style: 'mapbox://styles/mapbox/light-v10',
    center: point,
    zoom: 16,
    maxZoom: 16,
    minZoom: 16,
} );

let a = $( '.marker-1' ).html()
var marker = new mapboxgl.Marker( { color: '#c9ab81' } )
    .setLngLat( point )
    .setPopup( new mapboxgl.Popup( { className: 'my-Popup' } ) )
    .addTo( map );


$.extend( jQuery.validator.messages, {
    required: "必填",
    email: "請正確填寫您的 E-mail",
    number: "請輸入數字"
} )

$.validator.addMethod( "isMobile", function ( value, element )
{
    let length = value.length;
    let second_num = parseInt( value.split( '' )[ 1 ] )
    return this.optional( element ) || ( length == 10 && second_num == 9 );
}, "請正確填寫您的手機號碼" )

$( document ).ready( function ()
{
    $( 'form.contact_body__form' ).validate( {
        rules: {
            tel: {
                required: true,
                number: true,
                isMobile: true
            },
            email: {
                required: true,
                email: true
            }
        },
        errorElement: "em",
        errorClass: 'form-invalid-invert',
        highlight: function ( element, errorClass )
        {
            $( element ).removeClass( errorClass );
        },
        errorPlacement: function ( error, element )
        {
            error.insertAfter( element )
        }
    } )
} )