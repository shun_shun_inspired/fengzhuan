$( document ).ready( function ()
{
    $( '.fqa_note-block_hr' ).on( 'click', function ()
    {
        let target = $( this ).parent().next()
        let btn = $( this ).parent().find( '.fqa_note__section_head__toggle' )
        if ( target.hasClass( 'active' ) )
        {
            target.removeClass( 'active' )
            btn.removeClass( 'active' )
        } else
        {
            $( '.fqa_note__section_content' ).removeClass( 'active' )
            target.addClass( 'active' )

            $( '.fqa_note__section_head__toggle' ).removeClass( 'active' )
            btn.addClass( 'active' )
        }
    } )
} )