import { hiddenHeaderAndFootrt, backToprev } from '../function/func';
import $ from 'jquery'
import validate from 'jquery-validation'

hiddenHeaderAndFootrt()

$( '#sms-send' ).on( 'click touch', function ( e )
{
    if ( !$( 'form.pwd-reset_form' ).valid() ) return


    let that = this
    let start_time = 60
    $( that ).text( '剩餘' + start_time + 's' ).prop( 'disabled', 'disabled' )
    $( '.pwd-reset #captcha' ).attr( 'disabled', false )
    $( '.pwd-reset #account' ).attr( 'readonly', true )

    let _setInterval = setInterval( () =>
    {
        start_time--
        $( that ).text( '剩餘' + start_time + 's' )
    }, 1000 );

    setTimeout( () =>
    {
        clearInterval( _setInterval )
        $( that ).text( '取得驗證碼' ).prop( 'disabled', false )
        $( '.pwd-reset #account' ).attr( 'readonly', false )
    }, 60000 );
} )

$.extend( jQuery.validator.messages, {
    required: "必填",
    number: "請輸入數字",
    equalTo: "錯誤"
} )

$.validator.addMethod( "isMobile", function ( value, element )
{
    let length = value.length;
    let second_num = parseInt( value.split( '' )[ 1 ] )
    return this.optional( element ) || ( length == 10 && second_num == 9 );
}, "請正確填寫您的手機號碼" )

$( document ).ready( function ()
{
    $( 'form.login_form' ).validate( {
        rules: {
            account: {
                required: true,
                number: true,
                isMobile: true
            },
            password: {
                required: true,
            },
            comfirm_password: {
                equalTo: "[name='password']"
            }
        },
        errorElement: "em",
        errorClass: 'form-invalid',
        highlight: function ( element, errorClass )
        {
            $( element ).removeClass( errorClass );
        }
    } )

    $( 'form.pwd-reset_form' ).validate( {
        rules: {
            cellphone: {
                required: true,
                number: true
            },
            code: {
                required: true,
            }
        },
        errorElement: "em",
        errorClass: 'form-invalid',
        highlight: function ( element, errorClass )
        {
            $( element ).removeClass( errorClass );
        },
        errorPlacement: function ( error, element )
        {
            if ( element.attr( 'name' ) == 'cellphone' )
            {
                error.appendTo( $( element ).parent().parent() )
            }
            else
            {
                error.insertAfter( element )
            }
        }
    } )
} )