import { buildSwiper } from '../components/swiper'
import { spinButton } from '../function/func'
require("../components/loading")
require("../components/lazy")

$(window).on('load', function () {
    $('.header').css({
        'overflow': 'hidden',
        'animation': 'headerIn 1s forwards ease-out',
        'animation-delay': '8s',
        'transform': 'translate(0, -100%)',
    })
});

let indexBanner = buildSwiper('.index_swiper', {
    centeredSlides: true,
    loop: true,
    speed: 2000,
    watchSlidesProgress: true,
    keyboardControl: true,
    keyboard: true,
    grabCursor: true,
    effect: 'fade',
    observer: true,
    observeParents: true,
    fadeEffect: {
        crossFade: true
    },
    // autoplay: {
    //     delay: 3000
    // },
});

// 20210916 小丘新增 (將loading_page加入輪播，但等banner loading動畫結束再開始swiper)
setTimeout(function () { indexBanner.params.autoplay.delay = 3000; indexBanner.autoplay.start(); }, 12000);

buildSwiper('.index_products__swiper_container', {
    slidesPerView: 3,
    spaceBetween: '7%',
    observer: true,
    observeParents: true,
    navigation: {
        nextEl: '.index_products__swiper_next',
        prevEl: '.index_products__swiper_prev',
        hideOnClick: true,
        disabledClass: 'swiper-navigation-disabled',
    },
    breakpoints: {
        768: {
            slidesPerView: 'auto',
            spaceBetween: 30,
            direction: 'vertical',
            allowTouchMove: false
        },
    }
});


$(document).ready(function () {
    $('.main').addClass('short')

    $('.header_btn').addClass('white')
})


$(document).ready(function () {
    $('.index_swiper__scroll-down ').on('click', function () {
        $(document.documentElement, document.body).animate({
            scrollTop: $(".index_intro").offset().top
        }, 1000);
    })
})


$(document).ready(function () {
    $('.index_intro__side1-btn-blank').on('click touch', function () {
        $([document.documentElement, document.body]).animate({
            scrollTop: $(".index_products").offset().top
        }, 1000);
    })
})

$(document).ready(function () {
    $('.product-card_dropdown__title').on('click', function (e) {
        let select = $(this).next()
        if (!select.hasClass('active')) {
            $('.product-card_dropdown__select').removeClass('active')
            select.addClass('active')
        } else {
            select.removeClass('active')
        }

        $('body').on('click touch', function (event) {
            if (
                !$(event.target).closest('.product-card_dropdown').length &&
                !$(event.target).is('.product-card_dropdown') &&
                !$(event.target).closest('.product-card_dropdown__title').length &&
                !$(event.target).is('.product-card_dropdown__title')
            ) {
                $('.product-card_dropdown__select').removeClass('active')
            }
        })
    })

    $('.product-card_dropdown__select li').on('click', function () {
        let _this_val = $(this).text()
        let _this_price = $(this).data('price')
        let target_parent_parent = $(this).parent().parent().parent();
        let qty = target_parent_parent.find('bdi')
        let target_parent = $(this).parent().parent();
        let target = $(this).parent();
        let target_title = $(this).parent().prev();

        target.removeClass('active')
        target_parent.data('value', _this_val)
        target_title.text(_this_val)

        target.find('li').removeClass('active')
        $(this).addClass('active')
        target_parent_parent.find('.product-card_link__left').attr('data-originprice', _this_price).text(`$ ${_this_price}`)
        qty.text(1)
    })
})


$(document).ready(function () {
    $('.products_item_card-spinButton button').on('click', function () {
        let o_price_target = $(this).parent().parent().find('.product-card_link__left')
        let o_price = o_price_target.attr('data-originprice')
        let qty = 0, t_price = 0

        setTimeout(() => {
            qty = $(this).parent().find('bdi').text()
            t_price = parseInt(qty) * parseInt(o_price)
            o_price_target.text(`$ ${t_price}`)
        }, 100);
    })
})


spinButton()