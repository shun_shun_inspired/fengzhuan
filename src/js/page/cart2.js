import Cookies from 'js-cookie'
require( '../components/flatpickr' )
require( '../components/validate' )
require( '../components/zone_box' )


$( document ).ready( function ()
{
    $( $( '[name=order_zip]' ).parent().parent() ).zone_box( {
        MAIN_ID: "order_country",
        SUB_ID: "order_district",
        ZIP_ID: "order_zip",
        ZIP_GET: "",
        DEFAULT_AREA: "",
    } );

    $( $( '[name=reci_zip]' ).parent().parent() ).zone_box( {
        MAIN_ID: "reci_country",
        SUB_ID: "reci_district",
        ZIP_ID: "reci_zip",
        ZIP_GET: "",
        DEFAULT_AREA: "",
    } );

    $( '[name=order_zip]' ).trigger( 'keyup' );
    $( '[name=reci_zip]' ).trigger( 'keyup' );
} );


let form_o = Cookies.get( 'order_form' ) ? Cookies.get( 'order_form' ) : null

if ( form_o )
{
    form_o = JSON.parse( form_o )
    Object.keys( form_o ).map( ( e, i ) =>
    {
        let a = $( `[name="${ e }"]` )
        if ( a.length >= 0 )
        {
            if ( !form_o[ e ] || form_o[ e ].length <= 0 )
            {
                return
            } else if ( e.includes( 'addresses' ) )
            {
                let prefix = e.split( '_' )[ 0 ]
                $( `[name="${ prefix }_country"]` ).val( form_o[ e ][ 'country' ] )
                $( `[name="${ prefix }_district"]` ).val( form_o[ e ][ 'district' ] )
                $( `[name="${ prefix }_address"]` ).val( form_o[ e ][ 'address' ] )
                $( `[name="${ prefix }_zip"]` ).val( form_o[ e ][ 'zip' ] )
            } else
            {
                let type = a.prop( 'type' )
                if ( type == 'radio' )
                {
                    a.each( function ( i, el )
                    {
                        if ( $( el ).val() == form_o[ e ][ 'value' ] )
                        {
                            $( el ).prop( 'checked', 'checked' )

                            if ( e.includes( 'invoice_type' ) )
                            {
                                $( el ).parent().find( '[name*="ein-"]' ).val( form_o[ e + '_info' ][ 'value' ] )
                            }
                        }

                    } )
                } else
                {
                    a.val( form_o[ e ][ 'value' ] )
                }
            }
        }
    } )

    SELFORDELIVERY( $( 'input[name="shipment_type"]' ) )
}

$( 'input[name="shipment_type"]' ).on( 'change', function ()
{
    SELFORDELIVERY( this )
} )

$( 'input[name="same_orderer"]' ).on( 'change', function ( e )
{
    if ( $( this ).prop( 'checked' ) )
    {
        let a = $( '.form [name*="order_"]' )
        let names = {}
        a.each( function ( i, e )
        {
            let name = $( e ).prop( 'name' ).split( '_' )[ 1 ]
            let val = $( e ).val()
            names[ name ] = val
        } )

        Object.keys( names ).map( ( el, index ) =>
        {

            if ( el == 'zip' )
            {
                $( `.form [name="reci_${ el }"]` ).val( names[ el ] )
                $( `.form [name="reci_${ el }"]` ).trigger( 'keyup' )

                return
            }

            $( `.form [name="reci_${ el }"]` ).val( names[ el ] )
        } )
    }
} )

$( '.cart_submit' ).on( 'click', function ( e )
{
    let form = {
        payment_type: {
            text: $( '[name="payment_type"]:checked' ).parent().find( 'label' ).text(),
            value: $( '[name="payment_type"]' ).val(),
        },
        shipment_type: {
            text: $( '[name="shipment_type"]:checked' ).parent().find( 'label' ).text(),
            value: $( '[name="shipment_type"]' ).val(),
        },
        address: {
            text: $( '[name="address"]' ).prop( 'placeholder' ),
            value: $( '[name="address"]' ).prop( 'placeholder' ),
        },
        pickup_date: {
            text: $( '[name="pickup_date"]' ).val(),
            value: $( '[name="pickup_date"]' ).val(),
        },
        order_name: {
            text: $( '[name="order_name"]' ).val(),
            value: $( '[name="order_name"]' ).val(),
        },
        order_phone: {
            text: $( '[name="order_phone"]' ).val(),
            value: $( '[name="order_phone"]' ).val(),
        },
        order_addresses: {
            country: $( '[name="order_country"]' ).val(),
            district: $( '[name="order_district"]' ).val(),
            address: $( '[name="order_address"]' ).val(),
            zip: $( '[name="order_zip"]' ).val(),
            addresses: $( '[name="order_country"]' ).val() + $( '[name="order_district"]' ).val() + $( '[name="order_address"]' ).val() + ' ' + $( '[name="order_zip"]' ).val()
        },
        order_email: {
            text: $( '[name="order_email"]' ).val(),
            value: $( '[name="order_email"]' ).val(),
        },
        order_comment: {
            text: $( '[name="order_comment"]' ).val(),
            value: $( '[name="order_comment"]' ).val(),
        },
        reci_name: {
            text: $( '[name="reci_name"]' ).val(),
            value: $( '[name="reci_name"]' ).val(),
        },
        reci_phone: {
            text: $( '[name="reci_phone"]' ).val(),
            value: $( '[name="reci_phone"]' ).val(),
        },
        reci_addresses: {
            country: $( '[name="reci_country"]' ).val(),
            district: $( '[name="reci_district"]' ).val(),
            address: $( '[name="reci_address"]' ).val(),
            zip: $( '[name="reci_zip"]' ).val(),
            addresses: $( '[name="reci_country"]' ).val() + $( '[name="reci_district"]' ).val() + $( '[name="reci_address"]' ).val() + ' ' + $( '[name="reci_zip"]' ).val()
        },
        reci_email: {
            text: $( '[name="reci_email"]' ).val(),
            value: $( '[name="reci_email"]' ).val(),
        },
        reci_comment: {
            text: $( '[name="reci_comment"]' ).val(),
            value: $( '[name="reci_comment"]' ).val(),
        },
        invoice_type: {
            text: $( '[name="invoice_type"]:checked' ).parent().find( 'label' ).text(),
            value: $( '[name="invoice_type"]:checked' ).val(),
        },
        invoice_type_info: {
            text: $( '[name="invoice_type"]:checked' ).parent().find( '[name*="ein-"]' ).val(),
            value: $( '[name="invoice_type"]:checked' ).parent().find( '[name*="ein-"]' ).val(),
            name: $( '[name="invoice_type"]:checked' ).parent().find( '[name*="ein-"]' ).prop( 'name' ),
        },
    }

    Cookies.set( 'order_form', form, { expires: 1 } )
} )

function SELFORDELIVERY ( e )
{
    let target = $( e ).parent().parent().parent().parent().next()
    let deliverymax = $( e ).data( 'deliverymax' )
    let deliveryfee = $( e ).data( 'deliveryfee' )

    if ( $( e ).val() == 'DELIVERY' )
    {
        $( target ).find( '.col' ).eq( 0 ).hide()
        $( target ).find( '.col' ).eq( 1 ).addClass( 'full' )

        if ( $( '.delivery_fee' ).length )
        {
            let total_price = parseInt( $( '.cart_details li:last-child span:last-child' ).text().replace( 'NT$ ', '' ) )

            if ( total_price <= deliverymax )
            {
                $( '.delivery_fee' ).remove()

                total_price = total_price - deliveryfee
                $( '.cart_details li:last-child span:last-child' ).text( total_price )
            }
        }
    } else
    {
        $( target ).find( '.col' ).eq( 0 ).show()
        $( target ).find( '.col' ).eq( 1 ).removeClass( 'full' )

        if ( !$( '.delivery_fee' ).length )
        {
            let total_price = parseInt( $( '.cart_details li:last-child span:last-child' ).text().replace( 'NT$ ', '' ) )

            if ( total_price <= deliverymax )
            {
                let template = `<li class="delivery_fee"><span>運費：</span><span>$ 130</span></li>`
                $( template ).insertAfter( $( '.cart_details li:nth-child(1)' ) )

                total_price = total_price + deliveryfee
                $( '.cart_details li:last-child span:last-child' ).text( total_price )
            }
        }
    }
}