import { buildSwiper } from '../components/swiper'
import { spinButton,changeProductImage } from '../function/func'


buildSwiper( '.product_content__other_swiper__container', {
    slidesPerView: 3,
    spaceBetween: '7%',
    navigation: {
        nextEl: '.product_content__other_swiper__next',
        prevEl: '.product_content__other_swiper__prev',
        hideOnClick: true,
        disabledClass: 'swiper-navigation-disabled',
    },
    breakpoints: {
        768: {
            slidesPerView: 'auto',
            spaceBetween: 30,
            direction: 'vertical',
            allowTouchMove: false
        },
    }
} );

buildSwiper( '.product_content__swiper_container', {
    slidesPerView: 3,
    spaceBetween: '2%',
    navigation: {
        nextEl: '.product_content__swiper_next',
        prevEl: '.product_content__swiper_prev',
        hideOnClick: true,
        disabledClass: 'swiper-navigation-disabled',
    },
    breakpoints: {
        768: {
            slidesPerView: 'auto',
            spaceBetween: 30,
            direction: 'vertical',
            allowTouchMove: false
        },
    }
} );

let meun = [ '產品須知', '退換貨須知' ]
buildSwiper( '.product_content__note_list', {
    slidesPerView: 1,
    spaceBetween: 0,
    autoHeight: true,
    pagination: {
        el: '.product_content__note_pagination',
        clickable: true,
        renderBullet: function ( index, className )
        {
            return `<div class="product_content__note_title ${ className }">
                        <svg width="26" height="25" viewBox="0 0 26 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path class="icon" d="M23 5.5H20.275C20.4125 5.1125 20.5 4.6875 20.5 4.25C20.5 2.175 18.825 0.5 16.75 0.5C15.4375 0.5 14.3 1.175 13.625 2.1875L13 3.025L12.375 2.175C11.7 1.175 10.5625 0.5 9.25 0.5C7.175 0.5 5.5 2.175 5.5 4.25C5.5 4.6875 5.5875 5.1125 5.725 5.5H3C1.6125 5.5 0.5125 6.6125 0.5125 8L0.5 21.75C0.5 23.1375 1.6125 24.25 3 24.25H23C24.3875 24.25 25.5 23.1375 25.5 21.75V8C25.5 6.6125 24.3875 5.5 23 5.5ZM16.75 3C17.4375 3 18 3.5625 18 4.25C18 4.9375 17.4375 5.5 16.75 5.5C16.0625 5.5 15.5 4.9375 15.5 4.25C15.5 3.5625 16.0625 3 16.75 3ZM9.25 3C9.9375 3 10.5 3.5625 10.5 4.25C10.5 4.9375 9.9375 5.5 9.25 5.5C8.5625 5.5 8 4.9375 8 4.25C8 3.5625 8.5625 3 9.25 3ZM23 21.75H3V19.25H23V21.75ZM23 15.5H3V8H9.35L6.75 11.5375L8.775 13L11.75 8.95L13 7.25L14.25 8.95L17.225 13L19.25 11.5375L16.65 8H23V15.5Z" fill="#C9AB81"/>
                        </svg>
                        <p>${ meun[ index ] }</p>
                    </div>`;
        },
    },
} );


$( document ).ready( function ()
{
    $( 'input[name="spec_id"]' ).on( 'change', function ()
    {
        let price = $( this ).data( 'price' )
        let qty = $( '.product_content__top_right__add-spinButton input[type="number"]' ).val()
        let res_price = parseInt( qty ) * parseInt( price )
        $( '.product-card_link__left.product_content__top_right__add-card_link__left' ).text( `$ ${ res_price }` )
        $( '.product-card_link__left.product_content__top_right__add-card_link__left' ).attr( 'data-originprice', price )
    } )

    $( '.product_content__top_right__add-spinButton button' ).on( 'click', function ()
    {
        let target = $( '.product-card_link__left.product_content__top_right__add-card_link__left' )
        let o_price = target.attr( 'data-originprice' )
        let qty = 1
            , total = 0

        setTimeout( () =>
        {
            qty = $( this ).parent().find( 'input[type="number"]' ).val()
            total = parseInt( o_price ) * parseInt( qty )
            target.text( `$ ${ total }` )
        }, 100 );

    } )
} )


spinButton()

changeProductImage()