import { spinButton } from '../function/func'

$( document ).ready( function ()
{
    $( '.products_dropdown__toggle' ).on( 'click touch', function ()
    {
        let target = $( this ).next()
        if ( !target.hasClass( 'active' ) )
        {
            $( '.products_dropdown__meun' ).removeClass( 'active' )
            target.addClass( 'active' )
        } else
        {
            target.removeClass( 'active' )
        }
    } )

    $( 'body' ).on( 'click touch', function ( event )
    {
        if ( !$( event.target ).closest( '.products_dropdown__toggle' ).length && !$( event.target ).is( '.products_dropdown__toggle' ) )
        {
            $( '.products_dropdown__meun' ).removeClass( 'active' )
        }
    } )

} )

$( document ).ready( function ()
{
    let qty = $( '.products_dropdown-mb li' ).length
    let duration = Math.min( 36.36 * parseInt( qty ) * 1.5, 400 )
    $( '.products_dropdown-mb_title' ).on( 'click', function ()
    {
        if ( !$( this ).hasClass( 'active' ) )
        {
            $( this ).addClass( 'active' )
            $( this ).next().addClass( 'active' ).animate( {
                maxHeight: '118.93333vw',
                overflow: 'hidden',
                paddingTop: 0,
                paddingBottom: 0,
                marginTop: 0,
                marginBottom: 0
            }, duration )
        } else
        {
            $( this ).removeClass( 'active' )
            $( this ).next().animate( {
                maxHeight: '0px',
                overflow: 'hidden',
                paddingTop: 0,
                paddingBottom: 0,
                marginTop: 0,
                marginBottom: 0
            }, duration )
            setTimeout( () =>
            {
                $( this ).next().removeClass( 'active' )
            }, duration );
        }
    } )

    $( '.products_dropdown-mb li' ).on( 'click', function ()
    {
        window.location.href = $( this ).find( 'a' ).attr( 'href' )
        localStorage.setItem( 'products_dropdown-mb', [ $( this ).text(), $( this ).data( 'index' ) ] )
    } )
} )

$( document ).ready( function ()
{
    let current_cate = localStorage.getItem( 'products_dropdown-mb' ) ?
        localStorage.getItem( 'products_dropdown-mb' ).split( ',' ) :
        []

    if ( current_cate )
    {
        $( '.products_dropdown-mb_title' ).text( current_cate[ 0 ] )
        $( '.products_dropdown-mb li' ).removeClass( 'active' )
        $( '.products_dropdown-mb li[data-index=' + current_cate[ 1 ] + ']' ).addClass( 'active' )
    }
} )


$( document ).ready( function ()
{
    $( '.product-card_dropdown__title' ).on( 'click', function ( e )
    {
        let select = $( this ).next()
        if ( !select.hasClass( 'active' ) )
        {
            $( '.product-card_dropdown__select' ).removeClass( 'active' )
            select.addClass( 'active' )
        } else
        {
            select.removeClass( 'active' )
        }

        $( 'body' ).on( 'click touch', function ( event )
        {
            if (
                !$( event.target ).closest( '.product-card_dropdown' ).length &&
                !$( event.target ).is( '.product-card_dropdown' ) &&
                !$( event.target ).closest( '.product-card_dropdown__title' ).length &&
                !$( event.target ).is( '.product-card_dropdown__title' )
            )
            {
                $( '.product-card_dropdown__select' ).removeClass( 'active' )
            }
        } )
    } )

    $( '.product-card_dropdown__select li' ).on( 'click', function ()
    {
        let _this_val = $( this ).text()
        let _this_price = $( this ).data( 'price' )
        let target_parent_parent = $( this ).parent().parent().parent();
        let qty = target_parent_parent.find( 'bdi' )
        let target_parent = $( this ).parent().parent();
        let target = $( this ).parent();
        let target_title = $( this ).parent().prev();

        target.removeClass( 'active' )
        target_parent.data( 'value', _this_val )
        target_title.text( _this_val )

        target.find( 'li' ).removeClass( 'active' )
        $( this ).addClass( 'active' )
        target_parent_parent.find( '.product-card_link__left' ).attr( 'data-originprice', _this_price ).text( `$ ${ _this_price }` )
        qty.text( 1 )
    } )
} )

$( document ).ready( function ()
{
    $( '.products_item_card-spinButton button' ).on( 'click', function ()
    {
        let o_price_target = $( this ).parent().parent().find( '.product-card_link__left' )
        let o_price = o_price_target.attr( 'data-originprice' )
        let qty = 0, t_price = 0

        setTimeout( () =>
        {
            qty = $( this ).parent().find( 'bdi' ).text()
            t_price = parseInt( qty ) * parseInt( o_price )
            o_price_target.text( `$ ${ t_price }` )
        }, 100 );
    } )
} )



spinButton()