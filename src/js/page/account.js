import { hiddenHeaderAndFootrt, backToprev } from '../function/func';

hiddenHeaderAndFootrt()


$( document ).ready( function ()
{
    let pwd_comfirmed = $( '#pwd-comfirmed' )
    let pwd_comfirmed_container = $( '#pwd-comfirmed' ).parent().parent()
    pwd_comfirmed_container.hide()

    $( '.info_list__item_btn' ).on( 'click touch', function ()
    {
        let input = $( this ).prev()
        if ( input.attr( 'disabled' ) == 'disabled' )
        {
            input.attr( 'disabled', false )
            pwd_comfirmed.attr( 'disabled', false )
            input.attr( 'placeholder', '' )
            input.focus()
            pwd_comfirmed_container.show()
            $( this ).hide()
            $( this ).parent().prev().find( 'span' ).text( '新會員密碼' )
        } else
        {
            input.attr( 'disabled', 'disabled' )
            input.attr( 'placeholder', '●●●●●●●●●●' )
            pwd_comfirmed_container.hide()
            pwd_comfirmed.attr( 'disabled', 'disabled' )

        }
    } )
} )

$( document ).ready( function ()
{
    $( '.info_list__item_right__option' ).on( 'click touch', function ()
    {
        $( this ).find( 'input' ).attr( {
            'disabled': false,
        } ).addClass( 'active' )
    } )
} )