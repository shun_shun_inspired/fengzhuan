import flatpickr from "flatpickr";
import '../../../node_modules/flatpickr/dist/flatpickr.min.css';


$( document ).ready( function ()
{
    const fp = flatpickr( "#time", {
        dateFormat: 'Y/m/d',
        minDate: $( '#delivery_date' ).text()
    } );
} )