import * as dat from 'dat.gui';

const gui = new dat.GUI();
let rotate

var sampleRotate = function ()
{
    this.l_xr = 0
    this.l_yr = 0
    this.l_zr = 0
    this.l_tx = 0
    this.r_xr = 0
    this.r_yr = 0
    this.r_zr = 0
    this.r_tx = 0
};

$( document ).ready( function ()
{
    rotate = new sampleRotate();

    gui.add( rotate, 'l_xr', -360, 360 ).onChange( setValue );
    gui.add( rotate, 'l_yr', -360, 360 ).onChange( setValue );
    gui.add( rotate, 'l_zr', -360, 360 ).onChange( setValue );
    gui.add( rotate, 'r_xr', -360, 360 ).onChange( setValue );
    gui.add( rotate, 'r_yr', -360, 360 ).onChange( setValue );
    gui.add( rotate, 'r_zr', -360, 360 ).onChange( setValue );
} )

function setValue ()
{
    $( '.loading_left' ).css( {
        'transform': `rotateX(${ rotate.l_xr }deg) rotateY(${ rotate.l_yr }deg) rotateZ(${ rotate.l_zr }deg)`
    } )
    $( '.loading_right' ).css( {
        'transform': `rotateX(${ rotate.r_xr }deg) rotateY(${ rotate.r_yr }deg) rotateZ(${ rotate.r_zr }deg)`
    } )
}