import $ from 'jquery'
import validate from 'jquery-validation'

$.extend( jQuery.validator.messages, {
    required: "必填",
    email: "請正確填寫您的 E-mail",
    number: "請輸入數字"
} )

$.validator.addMethod( "isMobile", function ( value, element )
{
    let length = value.length;
    let second_num = parseInt( value.split( '' )[ 1 ] )
    return this.optional( element ) || ( length == 10 && second_num == 9 );
}, "請正確填寫您的手機號碼" )

$( document ).ready( function ()
{
    $( 'form.cart' ).validate( {
        rules: {
            reci_email: {
                email: true
            },
            order_email: {
                email: true
            },
            reci_phone: {
                isMobile: true,
                number: true
            },
            order_phone: {
                isMobile: true,
                number: true
            }
        },
        errorElement: "em",
        errorClass: 'form-invalid',
        highlight: function ( element, errorClass )
        {
            $( element ).removeClass( errorClass );
        },
        errorPlacement: function ( error, element )
        {
            if ( element.attr( 'type' ) == 'radio' || element.attr( 'type' ) == 'checkbox' )
            {
                error.appendTo( $( element ).parent().parent() )
            } else if ( /zip/.test( element.attr( 'name' ) ) )
            {
                error.appendTo( $( element ).parent().parent().parent() )
                $( element ).parent().css( {
                    'margin-bottom': 0
                } )
                error.css( {
                    'margin-bottom': '10px'
                } )
            }
            else if ( /address/.test( element.attr( 'name' ) ) )
            {
                error.appendTo( $( element ).parent().parent() )
            }
            else
            {
                error.insertAfter( element )
            }
        }
    } )
} )