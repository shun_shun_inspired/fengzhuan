import Swiper from '../../../node_modules/swiper/dist/js/swiper';
// import Swiper styles
import '../../../node_modules/swiper/dist/css/swiper.min.css';

export function buildSwiper ( ele, options )
{
    return new Swiper( ele, options )
}