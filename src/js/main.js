import "core-js";
import { Collapse } from "bootstrap"
require( "./components/swiper" )


$( document ).ready( function ()
{
    $( '.header_btn' ).on('click' , function ()
    {
        
        if ( $( '.header_toggle' ).hasClass( 'active' ) )
        {
            $( '.header' ).css({'overflow':'hidden'})
            $( '.header_toggle' ).removeClass( 'active' )
            $( this ).removeClass( 'active' )
            $( 'body' ).removeClass( 'is-open' )
        } else
        {
            $( '.header' ).css({'overflow':'visible'})
            $( 'body' ).addClass( 'is-open' )
            $( this ).addClass( 'active' )
            setTimeout( () =>
            {
                $( '.header_toggle' ).addClass( 'active' )
            }, 100 );
        }
    } )

} )

$( document ).ready( function ()
{
    $( document ).ready( function ()
    {
        $( window ).scroll( function ()
        {
            goTop_monitor()
        } );

        goTop_monitor()

        $( '.goTop' ).on( 'click touch', function ()
        {
            window.scrollTo( { top: 0, behavior: 'smooth' } );
        } )
    } )

    function goTop_monitor ()
    {
        let offset_top = $( '.goTop_monitor' ).offset().top
        if ( offset_top >= window.innerHeight )
        {
            $( '.goTop' ).addClass( 'active' )
        } else if ( offset_top < window.innerHeight )
        {
            $( '.goTop' ).removeClass( 'active' )
        }
    }
} )