export function hiddenHeaderAndFootrt() {
    $(document).ready(function () {
        $('.header,.footer').addClass('none')
        $('.main').addClass('none')
    })
}

export function backToprev() {
    $(document).ready(function () {
        $('.back.backToprev').on('click touch', function (e) {
            e.preventDefault();
            window.location.href = "index.html"
        })
    })
}

export function spinButton() {
    $('.spinButton .plus,.spinButton .less').on('click touch', function () {
        let qty = parseInt($(this).attr('value'))
        let current_qty = parseInt($(this).parent().find('input[type="number"]').val())

        let res = parseInt($(this).parent().find('input[type="number"]').val())
        if (current_qty <= 1 && qty == -1) {
            res = 1
        } else {
            res += qty
        }

        $(this).parent().find('input[type="number"]').val(res)
        countItemPrice(this, res)

    })
}

export function countItemPrice(element, qty) {
    let origin_price = $(element).parent().parent().parent().find('.origin_price').text()
    let item_total_price = parseInt(origin_price) * parseInt(qty)
    let item_total = $(element).parent().parent().parent().find('.item_total')
    item_total.text(item_total_price)
    countTotalSubPrice()
}

export function countTotalSubPrice() {
    let totalPrice = 0
    $('.cart-table_td').each(function (index, element) {
        let item_price = parseInt($(element).find('.item_total').text().trim());
        totalPrice += item_price
    })

    $('.order_sub_total_price').text(
        totalPrice
    )

    countDiscountPrice()
}

export function countDiscountPrice() {
    let discount = parseInt($('span.order_discount_price').text())
    let subTotal = parseInt($('.order_sub_total_price').text())
    let ratio = Math.floor(subTotal / discount)
    let qty = 0

    $('.cart-table_td').each(function (index, element) {
        qty += parseInt($(element).find('bdi').text().trim())
    })

    $('span.order_discount_price').text(qty * ratio)

    countTotalPrice()
}


export function countTotalPrice() {
    let sub_price = $('.order_sub_total_price').text()
    let discount_price = $('.order_discount_price').text()
    let total_price = $('.order_total_price')

    total_price.text(
        parseInt(sub_price) - parseInt(discount_price)
    )
}

export function cancelOrder(container) {
    $('.cart-table_cancel,.cart-table_cancel-mb').on('click touch', function () {
        let parent = $(this).parent().parent();

        $(parent).remove()
        countTotalSubPrice()
    })
}

//  改變產品圖片(20210908小丘)
export function changeProductImage() {
    // $('product-card_dropdown__title')
    $("input[name='spec_id']").on("click touch", function () {
        if ($(this).attr('data-img') === "") {
            return false
        }
        $(".product-card_image__bg > div").css({ 'backgroundImage': `url('${$(this).attr('data-img')}')` })
    })
}